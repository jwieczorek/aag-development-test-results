<?php
/**
 * Created by PhpStorm.
 * User: Joshua Wieczorek
 * Date: 4/3/2019
 * Time: 18:46
 *
 * @package TimedDelayLoop
 *
 * Notes: This small package is for PHP 7 and up as it uses PHP 7's new type hint features.
 */

/**
 * Class Random
 *
 * Description: This class is responsible for generating random objects.
 */
class Random
{
    /**
     * Generate an array of random numbers.
     *
     * @param int $length [the array length of random numbers.]
     *
     * @return array
     */
    public function numbers(int $length) : array
    {
        // Get maximum numbers range array.
        $numbers = range(0,$length);

        // Shuffle the numbers to mix them up.
        shuffle($numbers);

        // Return the array of numbers.
        return $numbers;
    }
}