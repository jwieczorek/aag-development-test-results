<?php
/**
 * Created by PhpStorm.
 * User: Joshua Wieczorek.
 * Date: 4/3/2019
 * Time: 18:45
 *
 * @package TimedDelayLoop
 *
 * Notes: This small package is for PHP 7 and up as it uses PHP 7's new type hint features.
 */

include './Random.php';

/**
 * Method to iterate through a loop of numbers and
 * print out the indexes sleeping 3 seconds before
 * writing the output.
 *
 * @param int $maxNumbersToLoopThrough [maximum iterations to loop through.]
 * @param int $waitTimeBetweenIterations [time to wait before moving the next in the iteration]
 */
function timedDelayLoop(int $maxNumbersToLoopThrough, int $waitTimeBetweenIterations)
{
    // Get random list of numbers to loop through.
    $numberList = (new Random())->numbers($maxNumbersToLoopThrough);

    // Loop through number list.
    foreach($numberList as $index => $number)
    {
        // Sleep before proceeding.
        sleep($waitTimeBetweenIterations);

        // Print number to output.
        echo $index . PHP_EOL;
    }

    // Print the loop is complete.
    echo "Loop iteration for a list of integers of numbers '{$maxNumbersToLoopThrough}' in length and waiting '{$waitTimeBetweenIterations}' seconds before print is complete." . PHP_EOL;
}

timedDelayLoop(10, 3);