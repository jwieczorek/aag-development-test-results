/**
 * Created by SSMS (Microsoft SQL Server Managment Studio).
 * User: Joshua Wieczorek.
 * Date: 4/3/2019
 * Time: 19:35
 */

 /*
  * Some personal comments:
  *
  *	The tables should really be normalized to reduce redundant data and prevent 
  * data integrity issues. For example, the same email address, name, etc. in 
  * multiple tables can be an issue if the columns are updated in one (1)
  * table but not anothers. 

  * Secondly, the tables should each have an IDENTITY (AI INT/BIGINT) column which
  * serves as the primary key. 

  * Additionally, if normailzing the data is not feasible, AND, if not already exists, 
  * an index needs to be placed on all email columns (and other joining columns) to 
  * increase performance and reduce the time SQL needs to scan the tables searching 
  *	for the correct adjoining records.
  */

-- 1. Write a query that lists all customer names and emails who serviced their vehicle (rodate), list in
-- ascending order.

SELECT		[name]
		,	[email]
FROM		[dbo].[DMS Service]
ORDER BY	[rodate] ASC


-- 2. Using the ELEADS Table, write a query that lists customerid, dealership, name, email, leadsource of
-- all customers whose rodate was in February. 

SELECT		[C].[customerid]
		,	[C].[dealership]
		,	[C].[name]
		,	[C].[email]
		,	[E].[leadsource]
FROM		[dbo].[ELEADS] [E]
	JOIN	[dbo].[CustomersTable] [C]
		ON	[E].[email] = [C].[email]
WHERE		[C].[rodate] BETWEEN '2019-02-01 00:00:00' AND '2019-02-28 23:59:59'


-- 3. Write a query that lists all customers (dealername, name, email ) who purchased a vehicle and
-- whose lead source type is equivalent to directmail. 

SELECT		[S].[dealername]
		,	[S].[name]
		,	[S].[email]
FROM		[dbo].[DMS Sales] [S]
	JOIN	[dbo].[ELEADS] [E]	
		ON	[S].[email] = [E].[email]
WHERE		[E].[leadsource] = 'direct mail' -- this is differnet that the instructions, however, this is what was displayed in the sample table results.


-- 4. Write a query that updates all customers with incorrect emails to NULL. 
-- Note, the where clause may not catch all invalid emails. There should be a SQL user defined function created to validate emails.

UPDATE		[C]
	SET		[C].[email] = NULL
FROM		[dbo].[Customers] [C]
WHERE		(CHARINDEX('@', [C].[email]) < 1)