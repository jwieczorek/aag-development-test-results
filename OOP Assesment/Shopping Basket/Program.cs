﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shopping_Basket.Core;

namespace Shopping_Basket
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Input / Output 1"
            CartItemsCollection cart1 = new CartItemsCollection();

            // Add books to cart item collection.
            CartItemCollection books = new CartItemCollection("Book", 12.49);
            books.QuantityAdd();
            cart1.AddToCart(books);

            // Add books to cart item collection.
            CartItemCollection musicCd = new CartItemCollection("Music CD", 14.99, true);
            cart1.AddToCart(musicCd);

            // Add books to cart item collection.
            CartItemCollection chocolateBar = new CartItemCollection("Chocolate bar", .85);
            cart1.AddToCart(chocolateBar);

            // Print receipt.
            Receipt receipt1 = cart1.GenerateReceipt();
            receipt1.Print();
            #endregion "Input / Output 1"


            Console.WriteLine("-----------------------------------------------");


            #region "Input / Output 2"
            CartItemsCollection cart2 = new CartItemsCollection();

            // Add chocolates to cart item collection.
            CartItemCollection chocolates1 = new CartItemCollection("Imported box of chocolates", 10.00, false, true);
            cart2.AddToCart(chocolates1);

            // Add perfume to cart item collection.
            CartItemCollection iPerfume1 = new CartItemCollection("Imported bottle of perfume", 47.50, true, true);
            cart2.AddToCart(iPerfume1);

            // Print receipt.
            Receipt receipt2 = cart2.GenerateReceipt();
            receipt2.Print();
            #endregion "Input / Output 2"


            Console.WriteLine("-----------------------------------------------");


            #region "Input / Output 3"
            CartItemsCollection cart3 = new CartItemsCollection();

            // Add perfume to cart item collection.
            CartItemCollection iPerfume2 = new CartItemCollection("Imported bottle of perfume", 27.99, true, true);
            cart3.AddToCart(iPerfume2);

            // Add perfume to cart item collection.
            CartItemCollection perfume2 = new CartItemCollection("Bottle of perfume", 18.99, true);
            cart3.AddToCart(perfume2);

            // Add pain killers cart item collection.
            CartItemCollection painKillers1 = new CartItemCollection("Packet of headache pills", 9.75);
            cart3.AddToCart(painKillers1);


            // Add chocolate box 1 to cart item collection.
            CartItemCollection iChocolateBox1 = new CartItemCollection("Imported box of chocolates", 11.25, false, true);
            iChocolateBox1.QuantityAdd();
            cart3.AddToCart(iChocolateBox1);           

            // Print receipt.
            Receipt receipt3 = cart3.GenerateReceipt();
            receipt3.Print();
            #endregion "Input / Output 3"


            Console.ReadLine();
        }
    }
}
