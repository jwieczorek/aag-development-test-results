﻿using System;
using Shopping_Basket.Contracts;

namespace Shopping_Basket.Core
{
    internal class CartItemCollection : ICartItemCollection
    {
        /// <summary>
        /// Quantity of items.
        /// </summary>
        public Int32 Quantity { get; private set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        public String ProductName { get; private set; }

        /// <summary>
        /// Individual price.
        /// </summary>
        public Double? IndividualPrice { get; private set; }

        /// <summary>
        /// Individual price with tax.
        /// </summary>
        public Double? IndividualPriceWithTax { get; private set; }

        /// <summary>
        /// Whether or not the item has local tax.
        /// </summary>
        public Boolean HasLocalTaxes { get; private set; }

        /// <summary>
        /// Whether or not the item is imported.
        /// </summary>
        public Boolean Imported { get; private set; }

        /// <summary>
        /// Line item taxes.
        /// </summary>
        public Double Taxes { get; private set; }

        /// <summary>
        /// The item's local tax.
        /// </summary>
        private Double _localTax { get; set; }

        /// <summary>
        /// The item's imort tax.
        /// </summary>
        private Double _importTax { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="individualPrice"></param>
        /// <param name="hasLocalTax"></param>
        /// <param name="isImported"></param>
        internal CartItemCollection(String productName, Double? individualPrice = null, Boolean hasLocalTax = false, Boolean imported = false)
        {
            Quantity = 1;
            Taxes = 0;
            ProductName = productName;
            IndividualPrice = individualPrice;
            HasLocalTaxes = hasLocalTax;
            Imported = imported;
            IndividualPriceWithTax = 0;
        }


        /// <summary>
        /// Add quantity to item collection.
        /// </summary>
        internal void QuantityAdd(Int32 qtyToAdd = 1)
        {
            Quantity = (Quantity + qtyToAdd);
        }


        /// <summary>
        /// Add quantity to item collection.
        /// </summary>
        internal void QuantityRemove(Int32 qtyToRemove = 1)
        {
            if (qtyToRemove > Quantity)
                throw new ArgumentException($"Cannot remove quantity '{qtyToRemove}' from item because item only has '{Quantity}' quantity.");

            Quantity = (Quantity - qtyToRemove);
        }


        /// <summary>
        /// Generate the total for this item collection.
        /// </summary>
        /// <returns></returns>
        public Double Total()
        {
            // Calculate the taxes.
            _calculateItemTaxes();

            // Set the individual price.
            IndividualPriceWithTax = Quantity > 1
                ? ((IndividualPrice + _roundTax((_localTax / Quantity)) + _roundTax((_importTax / Quantity))))
                : ((IndividualPrice + _localTax + _importTax));

            if(Quantity > 1)
                Taxes = ((IndividualPriceWithTax.Value * Quantity) - (IndividualPrice.Value * Quantity));

            // Return the calculated total.
            return (IndividualPriceWithTax.Value * Quantity);
        }


        /// <summary>
        /// Calculate the total without taxes.
        /// </summary>
        /// <returns></returns>
        public Double _total()
        {           
            return IndividualPrice.HasValue
                    ? ((IndividualPrice.Value * Quantity))
                    : 0;
        }


        /// <summary>
        /// Get the total ammount of the item collection with taxes.
        /// </summary>
        /// <returns></returns>
        private void _calculateItemTaxes()
        {
            if (HasLocalTaxes && IndividualPrice.HasValue)
            {
                Double taxes = TaxRates.GetLocalTaxRate();
                Double itemTax = (taxes * _total());
                _localTax = _roundTax(itemTax);
                Taxes = (Taxes + _localTax);
            }

            if (Imported && IndividualPrice.HasValue)
            {
                Double taxes = TaxRates.GetImportTaxRate();
                Double itemTax = (taxes * _total());
                _importTax = _roundTax(itemTax);
                Taxes = (Taxes + _importTax);
            }
        }


        /// <summary>
        /// Round the tax up.
        /// </summary>
        /// <param name="currentTax"></param>
        /// <returns></returns>
        private Double _roundTax(Double currentTax)
        {
            return (Math.Ceiling(currentTax / 0.05) * 0.05);
        }
    }
}
