﻿using System;

namespace Shopping_Basket.Core
{
    internal static class TaxRates
    {
        /// <summary>
        /// Get the local tax rate and return it.
        /// 
        /// This method is reading from the App.config,
        /// howerver, this could be a database call.
        /// </summary>
        /// <returns></returns>
        internal static Double GetLocalTaxRate()
        {
            // Get tax rate. This could be a database call instead
            // of the App.config.
            String taxRateString = Configs.ReadValue("LocalTaxRate");

            if (!String.IsNullOrEmpty(taxRateString))
            {
                Double taxRate;
                if (Double.TryParse(taxRateString, out taxRate))
                    return taxRate;

                throw new ArgumentException("Local tax rate is invalid. | Shopping_Basket.Core.TaxRates.GetLocalTaxRate");
            }
            else
                throw new ArgumentException("Local tax rate was not found. Cannot get local tax rate. | Shopping_Basket.Core.TaxRates.GetLocalTaxRate");
        }


        /// <summary>
        /// Get the import tax rate and return it.
        /// 
        /// This method is reading from the App.config,
        /// howerver, this could be a database call.
        /// </summary>
        /// <returns></returns>
        internal static Double GetImportTaxRate()
        {
            // Get tax rate. This could be a database call instead of
            // of the App.config.
            String taxRateString = Configs.ReadValue("ImportTaxRate");

            if (!String.IsNullOrEmpty(taxRateString))
            {
                Double taxRate;
                if (Double.TryParse(taxRateString, out taxRate))
                    return taxRate;

                throw new ArgumentException("Import tax rate is invalid. | Shopping_Basket.Core.TaxRates.GetImportTaxRate");
            }
            else
                throw new ArgumentException("Import tax rate was not found. Cannot get import tax rate. | Shopping_Basket.Core.TaxRates.GetImportTaxRate");
        }
    }
}
