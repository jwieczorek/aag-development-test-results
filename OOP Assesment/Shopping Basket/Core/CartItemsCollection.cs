﻿using System;
using System.Text;
using System.Collections.Generic;
using Shopping_Basket.Contracts;

namespace Shopping_Basket.Core
{
    internal class CartItemsCollection 
    {
        /// <summary>
        /// List of cart items.
        /// </summary>
        internal List<ICartItemCollection> CartItems { get; private set; }

        
        /// <summary>
        /// Constructor.
        /// </summary>
        internal CartItemsCollection()
        {
            CartItems = new List<ICartItemCollection>();
        }


        /// <summary>
        /// Add item to the cart items collection.
        /// </summary>
        /// <param name="item"></param>
        internal void AddToCart(ICartItemCollection item)
        {
            if (CartItems == null)
                CartItems = new List<ICartItemCollection>();

            CartItems.Add(item);
        }


        /// <summary>
        /// Generate and return the receipt object.
        /// </summary>
        /// <returns></returns>
        internal Receipt GenerateReceipt()
        {
            try
            {
                if (CartItems != null && CartItems.Count > 0)
                {
                    Receipt receipt = new Receipt();

                    // Loop through each cart item, calculate and add to the receipt.
                    CartItems.ForEach(item =>
                    {
                        Double total = item.Total();

                        StringBuilder sb = new StringBuilder();
                        sb.Append($"{item.ProductName}:");
                        sb.Append($" {total.ToString("N2")}");

                        if (item.Quantity > 1)
                            sb.Append($" ({item.Quantity} @ {item.IndividualPriceWithTax.Value.ToString("N2")})");    

                        // The line item text to the receipt.
                        receipt.LineItemText.Add(sb.ToString());

                        // Add the total to the receipt.
                        receipt.AddToTotal(total);

                        // Calculate the taxes and add to the receipt.
                        receipt.AddTaxes(item.Taxes);
                    });

                    return receipt;
                }

                return null;
            }
            catch (Exception ex)
            {
                Exception newEx = new Exception($"An error occurred while calculating the totals for the receipt.", ex);
                newEx.Data.Add("CartItems", CartItems);
                throw newEx;
            }            
        }
    }
}
