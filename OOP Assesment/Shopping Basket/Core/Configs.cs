﻿using System;
using System.Linq;
using System.Configuration;

namespace Shopping_Basket.Core
{
    internal static class Configs
    {
        /// <summary>
        /// Read value from the App.config.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static String ReadValue(String key)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
                return ConfigurationManager.AppSettings[key];

            return null;
        }
    }
}
