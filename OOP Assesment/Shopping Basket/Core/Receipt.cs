﻿using System;
using System.Collections.Generic;

namespace Shopping_Basket.Core
{
    internal class Receipt
    {
        /// <summary>
        /// List of line items text.
        /// </summary>
        internal List<String> LineItemText { get; set; }

        /// <summary>
        /// Total tax calculation.
        /// </summary>
        internal Double SalesTax { get; set; }

        /// <summary>
        /// Receipt total.
        /// </summary>
        internal Double Total { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        internal Receipt()
        {
            LineItemText = new List<String>();
            SalesTax = 0;
            Total = 0;
        }


        /// <summary>
        /// Add taxes to the total tax calculation.
        /// </summary>
        /// <param name="taxes"></param>
        internal void AddTaxes(Double taxes)
        {
            SalesTax += taxes;
        }


        /// <summary>
        /// Add amount to the total.
        /// </summary>
        /// <param name="amount"></param>
        internal void AddToTotal(Double amount)
        {
            Total += amount;
        }


        /// <summary>
        /// Print the receipt out.
        /// 
        /// This method could really send the data to the 
        /// printer, generate a PDF, or save to the database
        /// or all of the above.
        /// </summary>
        internal void Print()
        {
            foreach(String item in LineItemText)
            {
                Console.WriteLine(item);
            }

            // Print the sales tax to the output.
            Console.WriteLine($"Sales Tax {SalesTax.ToString("N2")}");

            // Print the total to the output.
            Console.WriteLine($"Total {Total.ToString("N2")}");
        }
    }
}
