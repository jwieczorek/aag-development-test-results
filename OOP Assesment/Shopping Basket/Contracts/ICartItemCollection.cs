﻿using System;

namespace Shopping_Basket.Contracts
{
    internal interface ICartItemCollection
    {
        /// <summary>
        /// Quantity of items.
        /// </summary>
        Int32 Quantity { get; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        String ProductName { get; }

        /// <summary>
        /// Individual price.
        /// </summary>
        Double? IndividualPrice { get; }

        /// <summary>
        /// Individual price with tax.
        /// </summary>
        Double? IndividualPriceWithTax { get; }

        /// <summary>
        /// Whether or not the item has local taxes.
        /// </summary>
        Boolean HasLocalTaxes { get; }

        /// <summary>
        /// Whether or not the item was imported.
        /// </summary>
        Boolean Imported { get; }

        /// <summary>
        /// Line item taxes.
        /// </summary>
        Double Taxes { get; }

        /// <summary>
        /// Get the total ammount of the item collection.
        /// </summary>
        /// <returns></returns>
        Double Total();
    }
}
